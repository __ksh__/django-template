FROM python:latest
MAINTAINER __ksh__

RUN apt-get update -y && apt-get install -y nginx supervisor less && \
rm -rf /var/lib/apt/lists/*
RUN python3 -m venv /opt/app
RUN /opt/app/bin/pip install --upgrade pip
RUN /opt/app/bin/pip install uwsgi
RUN mkdir -p /var/log/uwsgi /var/log/nginx /var/www/code
WORKDIR /var/www/code
ADD requirements.txt requirements.txt
RUN /opt/app/bin/pip install -r requirements.txt

COPY etc/nginx.conf /etc/nginx/sites-enabled/default
COPY etc/nginx-supervisor.conf  /etc/supervisor/conf.d/
COPY etc/uwsgi-supervisor.conf  /etc/supervisor/conf.d/
