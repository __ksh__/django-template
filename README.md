# README #

## setup docker-machine for MAC ##
```
xcode-select --install
ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"

brew cask install virtualbox
brew install docker docker-machine docker-compose

# create docker-machine
docker-machine create --driver "virtualbox" --virtualbox-cpu-count "1" \
--virtualbox-disk-size "15000"  --virtualbox-memory "1024" default
```

## start app ##
```
git clone https://bitbucket.org/__ksh__/django-template.git && cd django-template
touch etc/.env
bin/manage.py migrate
chmod -R 777 devel
echo yes|bin/manage.py collectstatic

#start docker-machine
docker-machine start && eval $(docker-machine env)
docker-compose build && docker-compose up -d
docker-machine ip
```
access docker-machine IP :)

## e.g ##

**create app**
```
mkdir plugins/business
bin/manage.py startapp business plugins/business
# ADD 'plugins.business' to INSTALLED_APPS
```

**generate er graph**
```
brew install graphviz
bin/manage.py graph_models -a -g > tmp/er.dot && dot -Tpng tmp/er.dot > tmp/er.png
```
looks like this.
![er.png](https://bitbucket.org/repo/dabxzEk/images/1429871885-er.png)