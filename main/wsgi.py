# WSGI config for app project.

"""
If you don't wanna use Newrelic, use these code.
"""

# import os

# from django.core.wsgi import get_wsgi_application

# os.environ.setdefault("DJANGO_SETTINGS_MODULE", "etc.settings")

# application = get_wsgi_application()


"""
If you wanna use Newrelic, use these code.
"""

import os
import newrelic.agent
from django.core.wsgi import get_wsgi_application
from django.conf import settings

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "etc.settings")
newrelic.agent.initialize('newrelic.ini', settings.NEWRELIC_ENV)
application = get_wsgi_application()
application = newrelic.agent.wsgi_application()(application)